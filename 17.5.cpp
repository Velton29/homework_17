﻿
#include <iostream>
#include <cmath>

class Example
{
private:
	int a;
public:
	Example() : a()
	{}

	Example(int newA) : a(newA)
	{}

	int GetA()
	{
		return a;
	}

	void SetA(int newA)
	{
		a = newA;
	}
};

class Vector
{
public:
    Vector()
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        std::cout << '\n' << x << ' ' << y << ' ' << z << '\n';
    }

    double x = 9.0;
	double x1 = 9.0;
    double y = 100.0;
	double y1 = 100.0;
    double z = 25.0;
	double z1 = 25.0;

public:
	Vector(double givX1, double givY1, double givX2, double givY2, double givZ1, double givZ2) : x(givX1), x1(givX2), y(givY1), y1(givY2), z(givZ1), z1(givZ2)
	{
		x = givX1;
		x1 = givX2;
		y = givY1;
		y1 = givY2;
		z = givZ1;
		z1 = givZ2;
	}

public:
	double Length()
	{
		return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	}
	void Show1()
	{
		std::cout << '\n' << sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2)) << '\n';
	}

	int GetX()
	{
		return x;
	}
};

int main()
{
	Vector v;
	v.Show();
	v.Show1();
	
	Example tepm(34);
	std::cout << '\n' << tepm.GetA();
}